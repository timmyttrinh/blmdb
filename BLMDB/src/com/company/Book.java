/**
 * @author Timmy Trinh
 * @since 03/01/2021
 * Book class, representation of an book record
 */
package com.company;

/**
 * Book
 * ----
 * This class holds a record of a book.
 *
 * @author
 */
public class Book extends Record {
    //    public String name;
//    public String description;
    public String author;
    public int year;

    /**
     * Constructors for objects of class Book
     */
    public Book() {
        // initialise instance variables
        super("Book", "unNamed Book", "blank description");
        this.author = "UnNamed author";
        this.year = 2021;
    }

    public Book(String title, String description, String author, int year) {
        // initialise instance variables
        super("Book", title, description);
        this.author = author;
        if (year < 1000 || year > 2021) year = 2021;
        else this.year = year;
    }

    /**
     * compareToBy
     * -----------
     * This method will compare this to other using the attribute specified by attributeToCompareBy.
     * A negative number is returned if this < other when comparing their respective attributeToCompareBy.
     * A 0 is returned if this = other with respect to attributeToCompareBy
     * a positive number is returned if this > other with respect to their attributeToCompareBy
     * So for example, if attributeToCompareBy is "Name", then
     * (assuming that both this and other have an attribute called 'Name')
     * if this.Name < other.Name, we'll return -1
     * if this.Name = other.Name, we'll return 0
     * if this.Name > other.Name, we'll return 1
     * PRECONDITION: Both this and other have the attribute specified by attributeToCompareBy.
     * If this has the attribute, but other does not, return a negative number.
     * If this doesn't have the attribute, return a positive number.
     * If neither has the attribute, return 0;
     * This strategy will make it so that the items that DO have the attributeToCompareBy
     * will end up at the beginning of a sorted list.
     * other must be a Record. If not, we'll return 0;
     * POSTCONDITION: Neither this nor other is changed in any way. An integer is returned
     */
    public int compareToBy(Record other, String attributeToCompareBy) {
        // [OH NO! IT GOT ERASED!]
        // all records hold a recordType, identifier, and description so call super class
        if (attributeToCompareBy.equalsIgnoreCase("recordtype") ||
                attributeToCompareBy.equalsIgnoreCase("identifier") ||
                attributeToCompareBy.equalsIgnoreCase("description")) {
            return super.compareToBy(other, attributeToCompareBy);
            // only books have authors
        } else if (attributeToCompareBy.equalsIgnoreCase("author")) {
            if(this.hasAttribute("author") && !other.hasAttribute("author")){
                return -1;
            } else if(!this.hasAttribute("author")) {
                return 1;
            } else if(!this.hasAttribute("author") && !other.hasAttribute("author")){
                return 0;
            } else {
                Book o = (Book) other;
                return this.getAuthor().compareToIgnoreCase(o.getAuthor());
            }
            // both books and movies have years
        } else if (attributeToCompareBy.equalsIgnoreCase("year")) {
            if(this.hasAttribute("year") && !other.hasAttribute("year")){
                return -1;
            } else if(!this.hasAttribute("year")) {
                return 1;
            } else if(!this.hasAttribute("year") && !other.hasAttribute("year")){
                return 0;
            } else {
                if(other.hasAttribute("author")) {
                    Book o = (Book) other;
                    return this.getYear() - o.getYear();
                } else {
                    Movie o = (Movie) other;
                    return this.getYear() - o.getYear();
                }

            }

        }
        return 1;
    }

    /**
     * getAuthor
     * ---------
     */
    public String getAuthor() {
        // [OH NO! IT GOT ERASED!]
        return this.author;
    }

    /**
     * getYear
     * -------
     * PRE: none
     * POST: return year int
     */
    public int getYear() {
        // [OH NO! IT GOT ERASED!]
        return this.year;
    }


    /**
     * toString
     * --------
     * This provides a nice looking String representation of this particular Record
     * PRE: none
     * POST: current object is unchanged. A String is returned.
     */
    public String toString() {
        return this.identifier + " (" + this.year + "), written by " + this.author + ": \n" + niceLookingDescription() + "\n";

    }
}

