/**
 * @author Timmy Trinh
 * @since 03/01/2021
 * RecordList class, holds records in an array
 */
package com.company;


/**
 * RecordList
 * ----------
 * RecordList is essentially an ArrayList of Records with a few sorting methods and display methods added.
 *
 * @author
 */
public class RecordList extends ArrayList<Record> {


    /**
     * Constructor for objects of class RecordList
     */
    public RecordList() {
        super();
    }


    /**
     * sortBy
     * ------
     * This sorts the Records by featureToSortBy.
     * This uses the Insertion Sort algorithm, since that one is stable
     * PRE: featureToSortBy describes one of the instance variables of at least one of the Records in the database,
     * such that it is taken into account by that Record's compareToBy method.
     * (If not, correct sorting is not guaranteed, but the program should not crash.)
     * POST: the records in the database are sorted in ascending order by the sorting criteria specified.
     */
    public void sortBy(String featureToSortBy) {
        // [OH NO! IT GOT ERASED!]
        if (featureToSortBy.equalsIgnoreCase("recordtype")) {
            sortByRecord();
        } else if (featureToSortBy.equalsIgnoreCase("identifier")) {
            sortByIdentifier();
        } else if (featureToSortBy.equalsIgnoreCase("description")) {
            sortByDescription();
        } else if (featureToSortBy.equalsIgnoreCase("author")) {
            sortByAuthor();
        } else if (featureToSortBy.equalsIgnoreCase("director")) {
            sortByDirector();
        } else if (featureToSortBy.equalsIgnoreCase("websiteURL")) {
            sortByWebsiteURL();
        } else if (featureToSortBy.equalsIgnoreCase("year")) {
            sortByYear();
        }
    }

    /*
     * // NOTE FROM KAREN:
     *
     * I got this Insertion Sort code from
     * https://stackabuse.com/insertion-sort-in-java/
     * (I'll use it as a template for making my own insertSort -- sortBy -- that works with my own ArrayList/RecordList)
     *
     * public static void insertionSort(int array[]) {
     *     for (int j = 1; j < array.length; j++) {
     *         int current = array[j];
     *         int i = j-1;
     *         while ((i > -1) && (array[i] > current)) {
     *             array[i+1] = array[i];
     *             i--;
     *         }
     *         array[i+1] = current;
     *     }
     * }
     */

    /**
     * sortByYear
     * <p>
     * functions as a helper method for sortBy
     * <p>
     * pre: none
     * post: sort by the attribute "year" in lexicographical order
     */
    public void sortByYear() {
        try {
            for (int i = 1; i < this.list.length; i++) {
                Record current = this.get(i);
                int j = i - 1;
                while (j > -1 && this.get(j).compareToBy(current, "year") > 0) {
                    overwrite(j + 1, this.get(j));
                    j--;
                }
                overwrite(j + 1, current);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }
    /**
     * sortByWebsiteURL
     * <p>
     * functions as a helper method for sortBy
     * <p>
     * pre: none
     * post: sort by the attribute "websiteURL" in lexicographical order
     */
    public void sortByWebsiteURL() {
        try {
            for (int i = 1; i < this.list.length; i++) {
                Record current = this.get(i);
                int j = i - 1;
                while (j > -1 && this.get(j).compareToBy(current, "websiteurl") > 0) {
                    overwrite(j + 1, this.get(j));
                    j--;
                }
                overwrite(j + 1, current);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    /**
     * sortByDirector
     * <p>
     * functions as a helper method for sortBy
     * <p>
     * pre: none
     * post: sort by the attribute "director" in lexicographical order
     */
    public void sortByDirector() {
        try {
            for (int i = 1; i < this.list.length; i++) {
                Record current = this.get(i);
                int j = i - 1;
                while (j > -1 && this.get(j).compareToBy(current, "director") > 0) {
                    overwrite(j + 1, this.get(j));
                    j--;
                }
                overwrite(j + 1, current);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    /**
     * sortByAuthor
     * <p>
     * functions as a helper method for sortBy
     * <p>
     * pre: none
     * post: sort by the attribute "author" in lexicographical order
     */
    public void sortByAuthor() {
        try {
            for (int i = 1; i < this.list.length; i++) {
                Record current = this.get(i);
                int j = i - 1;
                while (j > -1 && this.get(j).compareToBy(current, "author") > 0) {
                    overwrite(j + 1, this.get(j));
                    j--;
                }
                overwrite(j + 1, current);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    /**
     * sortByRecord
     * <p>
     * functions as a helper method for sortBy
     * <p>
     * pre: none
     * post: sort by the attribute "recordType" in lexicographical order
     */
    public void sortByRecord() {
        try {
            for (int i = 1; i < this.list.length; i++) {
                Record current = this.get(i);
                int j = i - 1;
                while (j > -1 && this.get(j).compareToBy(current, "recordtype") > 0) {
                    overwrite(j + 1, this.get(j));
                    j--;
                }
                overwrite(j + 1, current);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    /**
     * sortByIdentifier
     * <p>
     * functions as a helper method for sortBy
     * <p>
     * pre: none
     * post: sort by the attribute "identifier" in lexicographical order
     */
    public void sortByIdentifier() {
        try {
            for (int i = 1; i < this.list.length; i++) {
                Record current = this.get(i);
                int j = i - 1;
                while (j > -1 && this.get(j).compareToBy(current, "identifier") > 0) {
                    overwrite(j + 1, this.get(j));
                    j--;
                }
                overwrite(j + 1, current);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * sortByDescription
     * <p>
     * functions as a helper method for sortBy
     * <p>
     * pre: none
     * post: sort by the attribute "description" in lexicographical order
     */
    public void sortByDescription() {
        try {
            for (int i = 1; i < this.list.length; i++) {
                Record current = this.get(i);
                int j = i - 1;
                while (j > -1 && this.get(j).compareToBy(current, "description") > 0) {
                    overwrite(j + 1, this.get(j));
                    j--;
                }
                overwrite(j + 1, current);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * swap
     * ----
     * If you're gonna sort an ArrayList,
     * you might need a way to swap the elements of our ArrayList by index.
     * PRE: indices refer to elements in the arrayList.
     * POST: elements at indices a and b will have swapped places (the memory locations)
     */
    private void swap(int a, int b) {
        // [OH NO! IT GOT ERASED!]
        // [EVEN IF YOU DON'T END UP USING THIS METHOD IN YOUR SORT, CREATE IT.]
        try {
            Record temp = get(a);
            this.list[a] = get(b);
            this.list[b] = temp;
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * overwrite
     * ---------
     * This method overwrites the elements at index i with Record r,
     * so long as there is an element at index i.
     * PRE: there's an element at i
     * POST: overwrites element at i with r.
     */
    private void overwrite(int i, Record r) {
        // [OH NO! IT GOT ERASED!]
        // [EVEN IF YOU DON'T END UP USING THIS METHOD IN YOUR SORT, CREATE IT.]
        this.list[i] = r;
    }

    /**
     * displayAllRecords
     * -----------------
     * This prints to the screen a nice representation of all the records in their current order
     * Each record is on its own line.
     * PRE: none
     * POST: none.
     */
    public void displayAllRecords() {
        // [OH NO! IT GOT ERASED!]
        try {
            for (int i = 0; i < this.list.length; i++) {
                System.out.println();
                System.out.println("Record " + (i + 1) + " | " + get(i));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * displayRecordsOfRecordType()
     * ----------------------------
     * This prints to the screen a nice representation of all the records in the RecdordList
     * whose recordType matches the parameter recType.  It displays them in their current order.
     * Each record is on its own line.
     * PRE: r must be a recordType of at least some records, or else it will display no records.
     * POST: none
     */
    public void displayRecordsOfRecordType(String recType) {
        // [OH NO! IT GOT ERASED!]
        // [EVEN IF YOU DON'T END UP USING THIS METHOD IN YOUR SORT, CREATE IT.]
        try {
            for (int i = 0; i < this.list.length; i++) {
                if (get(i).getRecordType().equalsIgnoreCase(recType)) {
                    System.out.println();
                    System.out.println("Record " + (i + 1) + " | " + get(i));
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}
